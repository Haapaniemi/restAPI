<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Mita Tapahtuu</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
@include('inc.navbar')
@if(Request::is('/'))
    @include('inc.showcase')
@endif
<div id="topContainer" class="container">
    <div class="row">
        <div class="col">
            @include('inc.messages')

            @yield('dropdownbutton')
        </div>
    </div>
</div>
<footer id="footer" class="text-center">
    <p>Copyright 2018 &copy; MitaTapahtuu</p>
</footer>
</body>
</html>