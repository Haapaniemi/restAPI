@extends('layouts.app')

@section('content')
  <h1>Contact</h1>
  {!! Form::open(['url' => 'contact/submit']) !!}
  <div class="form-group">
    {{Form::label('name', 'Name')}}
    {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter name'])}}
  </div>
  <div class="form-group">
    {{Form::label('email', 'E-Mail Address')}}
    {{Form::text('email', '',['class' => 'form-control', 'placeholder' => 'Enter email'])}}
  </div>
  <div class="form-group">
    {{Form::label('message', 'Message')}}
    {{Form::text('message', '',['class' => 'form-control', 'placeholder' => 'Enter message'])}}
  </div>
  <div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
  </div>
  {!! Form::close() !!}
@endsection

@section('dropdownbutton')
<p>Valitse hakukriteerit ja suorita haku.</p>
<form>
  <div class="form-group">
    <label for="sel1">Valitse alue: </label>
    <select class="form-control" id="sel1">
      <option>Helsinki</option>
      <option>Vantaa</option>
      <option>Espoo</option>
      <option>Turku</option>
    </select>
    <label for="sel1">Valitse päivä: </label>

  </div>
</form>
  </div>
</div>
@endsection



@section('sidebar')
  @parent
  <p>This is appended to the sidebar</p>
@endsection
