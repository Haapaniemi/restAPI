
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VeeValidate from 'vee-validate';

window.Vue = require('vue');

Vue.use(VeeValidate)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('events', require('./components/Events.vue'));
Vue.component('navbar', require('./components/Navbar.vue'));
Vue.component('jumbotron', require('./components/Jumbotron.vue'));
Vue.component('footerbt', require('./components/Footer.vue'));


const app = new Vue({
    el: '#app'
});
