<?php

use Faker\Generator as Faker;

$factory->define(mitatapahtuu\Event::class, function (Faker $faker) {
    return [
        'name' => $faker->text(50),
        'creator' => $faker->text(5),
        'location' => $faker->text(10),
        'date' => $faker->dateTimeBetween('-30 days', '+30 days'),
        'description' => $faker->text(200),
    ];
});
