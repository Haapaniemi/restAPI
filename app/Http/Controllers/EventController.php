<?php

namespace mitatapahtuu\Http\Controllers;

use Illuminate\Http\Request;
use mitatapahtuu\Http\Requests;
use mitatapahtuu\Event;
use mitatapahtuu\Http\Resources\Event as EventResource;


class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get events
        $events = Event::orderBy('created_at', 'desc')->paginate(5);

        // Return collection of events as a resource
        return EventResource::collection($events);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $request->isMethod('put') ? Event::findOrFail
        ($request->event_id) : new Event;

        $event->id = $request->input('event_id');
        $event->name = $request->input('name');
        $event->creator = $request->input('creator');
        $event->location = $request->input('location');
        $event->description = $request->input('description');
        $event->date = $request->input('date');

        if($event->save()){
            return new EventResource($event);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get a single event
        $event = Event::findOrFail($id);

        // Return single event as resource
        return new EventResource($event);
    }

    public function filterLocation($location){
        //Get events
        $events = Event::where('location', $location)->get();

        // Return events
        return new EventResource($events);
    }

    public function filterDate($date){
        //Get events
        $events = Event::where('date', $date)->get();

        // Return events
        return new EventResource($events);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get a single event
        $event = Event::findOrFail($id);

        // Delete the event
        if($event->delete()) {
            return new EventResource($event);
        }
    }
}
